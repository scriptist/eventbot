var jwt     = require('jsonwebtoken');
var request = require('request'); // github.com/mikeal/request

module.exports = function(eb) {
	var _this = {
		auth: {},
		messageQueue: [],
		isSending: false,
		user: function(room, userId, callback) {
			_this._get(room, eb.config.hipchatUrl + '/v2/room/' + room.roomId + '/participant', {}, function (err, response, body) {
				var json = JSON.parse(body);
				var participants = json.items;

				var match = participants.find(function (participant) {
					return participant.id == userId;
				});

				callback(match);
			});
		},
		participants: function(room, callback) {
			_this._get(room, eb.config.hipchatUrl + '/v2/room/' + room.roomId + '/participant', {}, function (err, response, body) {
				var json = JSON.parse(body);
				var participants = json.items;
				callback(participants);
			});
		},
		send: function(room, message, isNotification) {
			if (_this.isSending) {
				return _this.messageQueue.push(Array.prototype.slice.call(arguments));
			}

			_this.isSending = true;
			_this._post(
				room,
				eb.config.hipchatUrl + '/v2/room/' + room.roomId + '/notification',
				{
					color: isNotification ? 'yellow' : (message.indexOf('Error') === 0 ? 'red' : 'green'),
					message: message,
					message_format: isNotification ? 'text' : 'html'
				},
				function(err, response, body) {
					if (err || !response || response.statusCode != 204) {
						console.log('Error sending to HipChat', err, response ? response.statusCode : null, body);
					}

					_this.isSending = false;

					if (_this.messageQueue.length) {
						var message = _this.messageQueue.shift();
						_this.send.apply(this, message);
					}
				}
			);
		},
		webhook: function(room, event, url, patternString, callback) {
			_this._post(
				room,
				eb.config.hipchatUrl + '/v2/room/' + room.roomId + '/webhook',
				{
					url: url,
					event: event,
					pattern: patternString
				},
				callback
			);
		},
		validateJWT: function(token, callback) {
			if (!token)
				return callback('No token provided.')

			var decoded = jwt.decode(token),
				room;

			if (!decoded)
				return callback('Invalid token provided.')

			eb.rooms.forEach(function(r) {
				if (r.oauthId == decoded.iss)
					room = r;
			});

			if (!room)
				return callback('That room does not exist.')

			jwt.verify(token, room.oauthSecret, function(err, decoded) {
				if (err) {
					return callback('Token does not match secret.')
				}
				
				return callback(room, decoded.sub);
			});
		},
		updateGlance: function(room, callback) {
			_this._post(
				room, 
				eb.config.hipchatUrl + '/v2/addon/ui/room/' + room.roomId, 
				{
					glance: [{
						key: "eventbot.glance",
						content: eb.event.upcomingGlanceJson(room)
					}]
				}, 
				callback
			);
		},
		_getAuthToken: function(room, callback) {
			// First check the auth cache
			if (room.roomId in _this.auth && _this.auth[room.roomId].expires > Date.create().getTime()) {
				typeof callback == 'function' && callback(_this.auth[room.roomId].token);
				return;
			}

			// Otherwise, get new token
			request.post(
				{
					url: eb.config.hipchatUrl + '/v2/oauth/token',
					headers: {
						'Authorization': 'Basic ' + new Buffer(room.oauthId + ':' + room.oauthSecret).toString('base64')
					},
					form: {
						grant_type: 'client_credentials',
						scope: eb.config.capabilities.capabilities.hipchatApiConsumer.scopes.join(' ')
					}
				},
				function(err, response, body) {
					if (err || !response || response.statusCode != 200) {
						console.log('Error sending to HipChat', err, response ? response.statusCode : null, body);
					}

					var json = JSON.parse(body);
					typeof callback == 'function' && callback(json.access_token);

					// Save to the auth cache
					_this.auth[room.roomId] = {
						token: json.access_token,
						expires: Date.create().addSeconds(json.expires_in).addMinutes(-1).getTime()
					}
				}
			);
		},
		_post: function(room, url, body, callback) {
			_this._getAuthToken(room, function(authToken) {
				request.post(
					url + '?auth_token=' + authToken,
					{
						json: body
					},
					function(err, response, body) {
						typeof callback == 'function' && callback(err, response, body);
					}
				);
			});
		},
		_get: function(room, url, params, callback) {
			_this._getAuthToken(room, function(authToken) {
				params.auth_token = authToken;
				request.get(
					url,
					{
						qs: params 
					},
					function(err, response, body) {
						typeof callback == 'function' && callback(err, response, body);
					}
				);
			});
		}
	};
	return _this;
}