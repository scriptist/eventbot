var jade = require('jade');
var socketIo = require('socket.io');

module.exports = function(eb) {
	var _this,
		listTemplate = jade.compileFile('./views/list.jade');

	_this = {
		io: null,
		init: function (app) {
			io = socketIo.listen(app);
		},
		updateSidebar: function (room) {
			events = eb.event.upcoming(room);

			events.forEach(function (event) {
				event.timePretty = eb.util.timeToString(event.time);

				if (event.places && event.places > event.people.length)
					event.remaining = event.places - event.people.length;
			});

			io.sockets.emit('update', listTemplate({
				events: events
			}));
		}
	}

	return _this;
}