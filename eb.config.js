/* Configuration */

var dev = process.env.DEV_MODE == 'true';
var url = "https://eventbot" + (dev ? '-dev' : '') + ".herokuapp.com";

module.exports = function(eb) {
	var _this = {
		versionInt: 1,
		url: url,
		repoUrl: 'https://bitbucket.org/scriptist/eventbot/',
		mongoEnabled: !!(process.env.MONGOLAB_URI || process.env.MONGOHQ_URL),
		mongoUri: process.env.MONGOLAB_URI || process.env.MONGOHQ_URL || null,
		eventExpiryMinutes: 60 * 12,
		archivePageExpiryMinutes: 15,
		hipchatUrl: 'https://api.hipchat.com',
		capabilities: {
			name: "EventBot" + (dev ? " Beta" : ""),
			description: "Create events simply from within HipChat",
			key: "com.scriptist.eventbot" + (dev ? 'dev' : ''),
			vendor: {
				name: "Scriptist",
				url: "http://scripti.st"
			},
			links: {
				homepage: url,
				self: url + "/capabilities"
			},
			capabilities: {
				installable: {
					callbackUrl: url + "/installable",
					allowGlobal: false
				},
				configurable: {
					url: url + "/configure"
				},
				hipchatApiConsumer: {
					scopes: [
						"send_notification",
						"admin_room",
						"view_room"
					]
				},
				glance: [{
					key: "eventbot.glance",
					name: {
						value: "Events"
					},
					queryUrl: url + "/glance",
					target: "eventbot.sidebar",
					icon: {
						url: url + "/static/icon/attending.png",
						"url@2x": url + "/static/icon/attending.png"
					}
				}],
				webPanel: [
					{
						key: "eventbot.sidebar",
						name: {
							value: "Events"
						},
						location: "hipchat.sidebar.right",
						url:  url + "/list"
					}
				]
			}
		}
	};
	return _this;
}
