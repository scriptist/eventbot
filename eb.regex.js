module.exports = function(eb) {
	var _this = {
		eventName: /^([^@]+?)$/i,
		userHandle: /^(?:@[^, -+_]+|me)$/i,
		userHandles: /(?:@[^, -+_]+|me)/ig,
		commandList: /^\/event(?:s)?(?:bot)?((?: [^|]*?)?(?:\|[^|]*?)*)(:?\/\/.*)?$/,
		command: {
			create: /^([^@]+?) ((?:(?:at|in|on|today|tomorrow|next) ([^,]+?))|now)(?:, ([0-9]+) (?:places|players))?$/i,
			rename: /^([^@]+?) to ([^@]+?)$/i,
			change: /^(time|name|places) of ([^@]+?) to ([^@]+?)$/i,
			add: /^((?:@[^, -+_]+|me)(?:(?:, ?| |,? and )(?:@[^, -+_]+|me))*) to ([^@]+?)$/i,
			remove: /^((?:@[^, -+_]+|me)(?:(?:, ?| |,? and )(?:@[^, -+_]+|me))*) from ([^@]+?)$/i,
			inviteFromPastEvents: /^([0-9]+|all) ([^@]+?) (?:attendees?|players?) to ([^@]+?)$/i
		}
	};
	return _this;
}