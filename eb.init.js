module.exports = function(eb) {
	return function() {
		eb.db.load(eb.reminder.init);
		eb.webserver.init();
		eb.plugin.init();
	};
}