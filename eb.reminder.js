module.exports = function(eb) {
	var _this = {
		interval: 5000,
		intervalId: null,
		init: function() {
			_this.process();
			_this.intervalId = setInterval(_this.process, _this.interval);
		},
		process: function() {
			var eventExpiry = Date.create(eb.config.eventExpiryMinutes + ' minutes ago');
			var operations = 0;

			eb.rooms.forEach(function(room) {
				var eventsExpired = false;
				room.events.forEach(function(evt, i) {

					// Expired events
					if (Date.create(evt.time).isBefore(eventExpiry)) {
						var evt = room.events.splice(i, 1)[0];

						room.eventArchive.push(evt);
						operations++;
						eventsExpired = true;
						return;
					}

					// Notify users of upcoming events
					evt.notifications.forEach(function(notification) {
						if (notification.notified)
							return;
						if (Date.create(notification.number + ' ' + notification.unit + ' from now').isBefore(evt.time))
							return;

						// Notify!
						var message = evt.name + ' is starting at ' + eb.util.timeToString(evt.time) + '.';
						evt.people.forEach(function(person) {
							message += ' @' + person;
						});
						eb.hipchat.send(room, message, true);

						notification.notified = true;
						operations++;
					});
				});

				if (eventsExpired) {
					// glance and sidebar are now stale, so update them

					// do not stop the event processing loop if we can't update a glance
					try {
						eb.hipchat.updateGlance(room, function (err, response, body) {
							if (err || !response || response.statusCode != 204) {
								console.log('GLANCE UPDATE FAILED', err, response ? response.statusCode : null, body);
							}
						});
					}
					catch (error) {
						console.log('GLANCE UPDATE FAILED', err);
					}

					// do not stop the event processing loop if we can't update a sidebar
					try {
						eb.websocket.updateSidebar(room);
					}
					catch (error) {
						console.log('SIDEBAR UPDATE FAILED', err);
					}
				}

				// Remove expired archive pages
				room.archivePages.forEach(function(archivePage, i) {
					if (Date.create(archivePage.expires).isPast()) {
						room.archivePages.splice(i, 1);
						operations++;
					}
				});
			});

			if (operations)
				eb.db.save();
		}
	};
	return _this;
};
