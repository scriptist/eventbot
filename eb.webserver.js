var express = require("express");
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var swig = require('swig');
var http = require('http');
var cors = require('cors');

module.exports = function(eb) {
	var _this = {
		app: null,
		cors: null,
		port: Number(process.env.PORT || 5000),
		init: function() {
			_this.app = express();
			
			_this.cors = cors({
				origin: /hipchat\.com$/
			});

			_this.app.use(bodyParser());

			// Web Socket
			var server = http.Server(_this.app)
			eb.websocket.init(server);

			// Template engine
			_this.app.set('views', './views');
			_this.app.set('view engine', 'jade');

			// Static files
			_this.app.use('/static', express.static(__dirname + '/static'));

			// Homepage
			_this.app.get('/', function(req, res) {
				res.sendfile(__dirname + '/pages/home.html');
			});

			// Config
			_this.app.get('/configure', function(req, res) {
				eb.hipchat.validateJWT(req.query.signed_request, function(result) {
					if (typeof result == 'string')
						return res.status(401).send('401 Error: Unauthorised. ' + result);

					res.send(swig.renderFile(__dirname + '/pages/configure.html', {room: result, plugins: eb.plugin.plugins}));
				});
			});
			_this.app.post('/configure', function(req, res) {
				eb.hipchat.validateJWT(req.query.signed_request, function(result) {
					if (typeof result == 'string')
						return res.status(401).send(result);

					var room = result;

					// Plugins
					var plugins = req.body.plugins;
					if (typeof plugins === 'string')
						plugins = [plugins];
					else if (typeof plugins !== 'object')
						plugins = [];

					room.plugins = {};
					plugins.forEach(function(plugin) {
						room.plugins[plugin] = true;
					});

					// Notifications
					var numbers, units;

					if (!('number' in req.body) || !('unit' in req.body)) {
						numbers = [];
						units = [];
					} else if (typeof req.body.number == 'string') {
						numbers = [req.body.number];
						units = [req.body.unit];
					} else {
						numbers = req.body.number;
						units = req.body.unit;
					}

					var newNotifications = [];
					for (var i = 0; i < numbers.length && i < units.length; i++) {
						newNotifications.push({
							number: parseInt(numbers[i]),
							unit: units[i]
						});
					};

					room.notifications = newNotifications;

					eb.db.save();

					res.send(swig.renderFile(__dirname + '/pages/configure.html', {room: room, plugins: eb.plugin.plugins, saved: true}));
				});
			});

			// Install redirect
			_this.app.get('/install', function(req, res) {
				res.redirect('https://www.hipchat.com/addons/install?url=' + eb.config.url + '/capabilities');
			});

			// Capabilities JSON
			_this.app.get('/capabilities', function(req, res) {
				res.type('application/json');
				res.send(JSON.stringify(eb.config.capabilities, null, 4));
			});

			// Installation
			_this.app.post('/installable', function(req, res) {
				// First make sure the room doesn't already exist
				var existingRoom;
				eb.rooms.forEach(function(room, i) {
					if (room.roomId == req.body.roomId) {
						existingRoom = eb.rooms.splice(i, 1)[0];
					}
				});

				// Then add it
				var room = {
					roomId: req.body.roomId,
					groupId: req.body.groupId,
					oauthId: req.body.oauthId,
					oauthSecret: req.body.oauthSecret,
					capabilitiesUrl: req.body.capabilitiesUrl,
				};
				if (existingRoom) {
					room.notifications = existingRoom.notifications;
					room.archivePages = existingRoom.archivePages;
					room.invites = existingRoom.invites;
					room.events = existingRoom.events;
					room.eventArchive = existingRoom.eventArchive;
					room.plugins = existingRoom.plugins;
				} else {
					room.notifications = [{
						number: 10,
						unit: 'minutes'
					}];
					room.archivePages = [];
					room.invites = {};
					room.events = [];
					room.eventArchive = [];
				}
				eb.rooms.push(room);

				// And save the database
				eb.db.save();

				delete eb.hipchat.auth[room.roomId];

				// Now make the request to add the webhook
				eb.hipchat.webhook(
					room,
					'room_message',
					eb.config.url + '/webhook', '^\/event(s)?(bot)?( .*)?$',
					function(err, response, body) {
						if (!err && response.statusCode == 201 && body.id) {
							res.send('Added successfully');

							// Message room
							eb.hipchat.send(room, 'Wowza, it looks like a shiny new bot has been installed! Type /event help for more info.', true);
						} else {
							res.status(404).send('Unable to register webhook. Try again.');
							console.log('WEBHOOK FAILED', err, response.statusCode, body);
						}
					}
				);
			});

			// Webhooks
			_this.app.post('/webhook', function(req, res) {
				res.send();

				// Get room info
				var room;
				eb.rooms.forEach(function(r) {
					if (r.roomId == req.body.item.room.id)
						room = r;
				});
				if (!room)
					return;

				// Split message into separate commands
				var message = req.body.item.message.message,
					matches = message.match(eb.regex.commandList);

				var commands = matches[1].split('|');

				// Execute commands 1s apart
				var i = 0;

				commands.forEach(function(command) {
					command = command.trim().replace(/ {2,}/, ' ');

					if (command.indexOf("/event") === 0 || command.indexOf("event") === 0)
						command = command.substr(command.indexOf(' ') + 1);

					var firstSpace = command.indexOf(' ') == -1 ? command.length : command.indexOf(' ');
					var commandName = command.substr(0, firstSpace),
						commandArgs = command.substr(firstSpace + 1);

					if (!commandName)
						commandName = 'list';

					var commandFunc = eb.util.getCommand(commandName, room);

					commandFunc(room, commandArgs, req.body.item);
				});
			});

			// Archive page
			_this.app.get('/archive/:key', function (req, res) {
				var key = req.params.key,
					room = eb.util.getRoomFromArchiveId(key);

				if (room) {
					room.eventArchive.sort(function(a, b) {
						return b.time - a.time;
					});

					// Clone it so the template doesn't modify it (e.g. |reverse filter)
					room = eb.util.clone(room);
				} else {
					res.status(404);
				}

				res.send(swig.renderFile(__dirname + '/pages/archive.html', {
					key: key,
					room: room,
					archivePageExpiryMinutes: eb.config.archivePageExpiryMinutes,
					timeToString: eb.util.timeToString
				}));
			});

			// Secret message page
			_this.app.get('/msg/:id', function (req, res) {
				var id = req.params.id,
					room;

				eb.rooms.forEach(function(r) {
					if (r.roomId == id)
						room = r;
				});

				if (!room)
					return res.status(404).send('No room found');

				res.send('<form method="post"><input type="text" name="message"><br /><input type="password" name="password"><br /><button>Submit</button></form>');
			});
			_this.app.post('/msg/:id', function (req, res) {
				var id = req.params.id,
					room;

				eb.rooms.forEach(function(r) {
					if (r.roomId == id)
						room = r;
				});

				if (!room)
					return res.status(404).send('No room found');

				if (eb.util.hash(req.body.password) != -1153854327)
					return res.status(404).send('Invalid password');

				eb.hipchat.send(room, req.body.message, true);

				res.send('<script>location.href = location.href;</script>');
			});
			_this.app.get('/glance', _this.cors, function (req, res) {
				eb.hipchat.validateJWT(req.query.signed_request, function(result) {
					var events;
					if (typeof result == 'string')
						return res.status(401).send(result);

					var room = result;

					res.type('application/json');
					res.send(JSON.stringify(eb.event.upcomingGlanceJson(room), null, 4));
				});
			});
			_this.app.get('/list', _this.cors, function (req, res) {
				eb.hipchat.validateJWT(req.query.signed_request, function(result, userId) {
					var events;
					if (typeof result == 'string')
						return res.status(401).send(result);

					var room = result;
					events = eb.event.upcoming(room);

					events.forEach(function (event) {
						event.timePretty = eb.util.timeToString(event.time);

						if (event.places && event.places > event.people.length)
							event.remaining = event.places - event.people.length;
					});

					res.type('text/html');
					res.render('list-static', {
						events: events,
						token: req.query.signed_request,
						currentUserId: userId
					});
				});
			});
			_this.app.post('/create', function (req, res) {
				eb.hipchat.validateJWT(req.query.signed_request, function(result, userId) {
					if (typeof result == 'string')
						return res.status(401).send(result);

					var room = result;
					var name = req.body.name;
					var time = Date.create(req.body.time).getTime();
					var places = parseInt(req.body.places);
					places = isNaN(places) ? null : places;
					var invites = req.body.invites || [];

					var result = eb.event.create(room, name, time, places);
					if (typeof result == 'string')
						return res.status(400).send(result);

					eb.hipchat.user(room, userId, function (user) {
						eb.event.addUsers(room, name, [user.mention_name]);
						eb.event.inviteUsers(room, name, invites, user.mention_name);

						eb.hipchat.send(room, 'Event created: ' + eb.event.htmlInfo(result, 0));
						eb.hipchat.updateGlance(room, function (err, response, body) {
							if (err || response.statusCode != 204) {
								console.log('GLANCE UPDATE FAILED', err, response.statusCode, body);
							}
						});
						eb.websocket.updateSidebar(room);

						if (invites.length) {
							var mentions = invites.reduce(function (val, invite) {
								if (val) {
									val += ' '
								}
								return val += '@' + invite;
							}, '');
							eb.hipchat.send(room, mentions + ' you have been invited to ' + name, true);
						}
					});

					res.status(204).send('Done');
				});
			});
			_this.app.get('/participants', function (req, res) {
				eb.hipchat.validateJWT(req.query.signed_request, function(result) {
					var events;
					if (typeof result == 'string')
						return res.status(401).send(result);

					var room = result;
					
					eb.hipchat.participants(room, function (participants) {
						res.type('application/json');
						res.send(JSON.stringify(participants, null, 4));
					});
				});
			});

			server.listen(_this.port, function() {
				console.log("Listening on " + _this.port);
			});
		}
	};
	return _this;
}