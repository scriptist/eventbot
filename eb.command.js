module.exports = function(eb) {
	var _this = {
		list: function(room, message, item) {
			eb.hipchat.send(room, eb.event.htmlList(room));
		},
		info: function(room, message, item) {
			var evts = eb.event.getEventsBySubstring(room, message);

			eb.hipchat.send(room, eb.event.htmlList(evts, {categorise: false, extended: true}));
		},
		mine: function(room, message, item) {
			eb.hipchat.send(room, eb.event.htmlMine(room, eb.util.getHandle(item)));
		},
		today: function(room, message, item) {
			eb.hipchat.send(room, eb.event.htmlToday(room));
		},
		create: function(room, message, item) {
			var matches = message.match(eb.regex.command.create);

			if (!matches)
				return eb.hipchat.send(room, 'Error: This is the correct syntax: /event create [event] at [time], [number] places');

			var evtName = matches[1],
				evtTimeStr = matches[2].match(/^(at|on)/) ? matches[3] : matches[2],
				evtPlaces = parseInt(matches[4]) > 0 ? parseInt(matches[4]) : null;

			// Clever time
			while (evtTimeStr.split(' ').length > 1 && !Date.create(evtTimeStr).isValid()) {
				var a = evtTimeStr.split(' ');
				evtName += ' ' + a.splice(0, 1)[0];

				if (a[0].match(/^(at|on)/) && Date.create(a.slice(1).join(' ')).isValid())
					a.splice(0, 1);
				evtTimeStr = a.join(' ');
			}

			var evtTime = Date.create(evtTimeStr).getTime()

			var result = eb.event.create(room, evtName, evtTime, evtPlaces);

			// Error
			if (typeof result == 'string')
				return eb.hipchat.send(room, 'Error: ' + result);

			eb.event.addUsers(room, evtName, [item.message.from.mention_name]);

			eb.hipchat.send(room, 'Event created: ' + eb.event.htmlInfo(result, 0));
			eb.hipchat.updateGlance(room, function (err, response, body) {
				if (err || response.statusCode != 204) {
					console.log('GLANCE UPDATE FAILED', err, response.statusCode, body);
				}
			});
			eb.websocket.updateSidebar(room);
		},
		cancel: function(room, message, item) {
			var result = eb.event.cancel(room, message);

			if (typeof result == 'string')
				return eb.hipchat.send(room, 'Error: ' + result);

			var notification = '"' + result.name + '" has been cancelled.';
			result.people.forEach(function(person) {
				notification += ' @' + person;
			});

			eb.hipchat.send(room, notification, true);
			eb.hipchat.updateGlance(room, function (err, response, body) {
				if (err || response.statusCode != 204) {
					console.log('GLANCE UPDATE FAILED', err, response.statusCode, body);
				}
			});
			eb.websocket.updateSidebar(room);
		},
		rename: function(room, message, item) {
			if (!message.match(eb.regex.command.rename))
				return eb.hipchat.send(room, 'Error: This is the correct syntax: /event rename [old event] to [new event]');
			return _this.change(room, 'name of ' + message, item);
		},
		change: function(room, message, item) {
			var matches = message.match(eb.regex.command.change);

			if (!matches)
				return eb.hipchat.send(room, 'Error: This is the correct syntax: /event change [property] of [event] to [value]');

			var evtName = matches[2],
				property = matches[1],
				value = matches[3];

			if (property == 'time')
				value = Date.create(value).getTime();
			else if (property == 'places')
				value = parseInt(value) > 0 ? parseInt(value) : null;

			var result = eb.event.change(room, evtName, property, value);

			if (typeof result == 'string')
				return eb.hipchat.send(room, 'Error: ' + result);

			eb.hipchat.send(room, 'Event modified: ' + eb.event.htmlInfo(result));
		},
		join: function(room, message, item) {
			var eventName = message;
			// if there is exactly one event then join it without making the user type in the only possibility
			// otherwise error
			if (!message.match(eb.regex.eventName)) {
				var upcoming = eb.event.upcoming(room);
				if (upcoming.length == 1)
					eventName = upcoming[0].name;
				else
					return eb.hipchat.send(room, 'Error: This is the correct syntax: /event join [event]');
			}
			return _this.add(room, 'me to ' + eventName, item);
		},
		leave: function(room, message, item) {
			if (!message.match(eb.regex.eventName))
				return eb.hipchat.send(room, 'Error: This is the correct syntax: /event leave [event]');
			return _this.remove(room, 'me from ' + message, item);
		},
		add: function(room, message, item) {
			var matches = message.match(eb.regex.command.add);

			if (!matches)
				return eb.hipchat.send(room, 'Error: This is the correct syntax: /event add @[handle] to [event]');

			var users = eb.util.stringToHandles(matches[1], item),
				evtName = matches[2];

			var result = eb.event.addUsers(room, evtName, users);

			if (typeof result == 'string')
				return eb.hipchat.send(room, 'Error: ' + result);

			eb.hipchat.send(room, 'User' + (users.length > 1 ? 's' : '') + ' added: ' + eb.event.htmlInfo(result));
			eb.websocket.updateSidebar(room);
		},
		remove: function(room, message, item) {
			var matches = message.match(eb.regex.command.remove);

			if (!matches)
				return eb.hipchat.send(room, 'Error: This is the correct syntax: /event remove @[handle] from [event]');

			var users = eb.util.stringToHandles(matches[1], item),
				evtName = matches[2];

			var result = eb.event.removeUsers(room, evtName, users);

			if (typeof result == 'string')
				return eb.hipchat.send(room, 'Error: ' + result);

			eb.hipchat.send(room, 'User' + (users.length > 1 ? 's' : '') + ' removed: ' + eb.event.htmlInfo(result));
			eb.websocket.updateSidebar(room);
		},
		invite: function(room, message, item) {
			var matches = message.match(eb.regex.command.add);

			if (!matches) {
				if (message.match(eb.regex.command.inviteFromPastEvents)) {
					return _this.inviteFromPastEvents(room, message, item);
				}
				return eb.hipchat.send(room, 'Error: This is the correct syntax: /event invite @[handle] to [event]');
			}

			var users = eb.util.stringToHandles(matches[1], item),
				evtName = matches[2];

			for (var i = 0; i < users.length; i++) {
				if (users[i] == 'here' || users[i] == 'all')
					return eb.hipchat.send(room, 'You cannot invite @here or @all to an event.');
			};

			var result = eb.event.inviteUsers(room, evtName, users, eb.util.getHandle(item));

			if (typeof result == 'string')
				return eb.hipchat.send(room, 'Error: ' + result);

			var message = '';
			users.forEach(function(user, i) {
				message += '@' + user;
				if (i < users.length - 2)
					message += ', ';
				else if (i < users.length - 1)
					message += ' and '
			});

			message += ' ' + (users.length > 1 ? 'have' : 'has')+' been invited to ' + result.name + ' at ' + eb.util.timeToString(result.time) + '. Type \'/event accept\' to accept this invitation.';

			eb.hipchat.send(room, message, true);
		},
		inviteFromPastEvents: function(room, message, item) {
			var matches = message.match(eb.regex.command.inviteFromPastEvents);

			if (!matches)
				return eb.hipchat.send(room, 'Error: This is the correct syntax: /event invite @[handle] to [event]');

			var count   = matches[1] === 'all' ? -1 : parseInt(matches[1]),
				likeEvt = matches[2],
				evtName = matches[3],
				evts    = eb.event.getEventsBySubstring(room, evtName),
				evt     = evts.length === 1 ? evts[0] : null;

			var users = eb.event.getUsersFromPastEvents(room, likeEvt);

			if (evt) {
				var removeHandle = function(handle) {
					var index = users.indexOf(handle);
					if (index !== -1)
						users.splice(index, 1);
				}
				evt.people.forEach(removeHandle);
				evt.invites && evt.invites.forEach(function(invite) {
					removeHandle(invite.user);
				});
			}

			if (count >= 0)
				users.splice(count);

			if (users.length === 0)
				return eb.hipchat.send(room, 'Error: no users found');

			var fakeCommand = '';
			users.forEach(function(handle) {
				fakeCommand += '@' + handle + ' ';
			});
			fakeCommand += 'to ' + evtName;

			return _this.invite(room, fakeCommand, item);
		},
		accept: function(room, message, item) {
			var result = eb.event.acceptInvite(room, eb.util.getHandle(item));

			if (typeof result == 'string')
				return eb.hipchat.send(room, 'Error: ' + result);

			eb.hipchat.send(room, 'Invitation accepted: ' + eb.event.htmlInfo(result));
			eb.websocket.updateSidebar(room);
		},
		decline: function(room, message, item) {
			var result = eb.event.declineInvite(room, eb.util.getHandle(item));

			if (typeof result == 'string')
				return eb.hipchat.send(room, 'Error: ' + result);

			var notification = '@' + eb.util.getHandle(item) + ' declined @' + result.invite.from + '\'s invitation to ' + result.evt.name;
			if (message.length)
				notification += ': ' + message.replace(/^(because|'?cause|cos) /, '');

			eb.hipchat.send(room, notification, true);
			eb.websocket.updateSidebar(room);
		},
		reject: function(room, message, item) {
			return _this.decline.apply(this, arguments);
		},
		archive: function(room, message, item) {
			// Create archive page

			var archive = {
				id: null,
				expires: Date.create().addMinutes(eb.config.archivePageExpiryMinutes).getTime()
			};

			while (!archive.id || eb.util.getRoomFromArchiveId(archive.id))
				archive.id = Math.floor(Math.random()*1000000);

			room.archivePages.push(archive);
			eb.db.save();

			// Send URL
			eb.hipchat.send(room, '<a href="'+eb.config.url+'/archive/'+archive.id+'">Your archive page has been generated, and will expire in '+eb.config.archivePageExpiryMinutes+' minutes.</a>');
		},
		chastise: function(room, message, item) {
			if (!message.match(eb.regex.userHandle))
				return eb.hipchat.send(room, 'Error: This is the correct syntax: /event chastise @[handle]');

			var handle = eb.util.stringToHandles(message, item)[0];

			var messages = [
				'{handle} is a very naughty child.',
				'Stop messing around {handle}.',
				'Do I have to get my whip out {handle}?',
				'Now now {handle}, can\'t you save your nonsense for later?',
				'Siigghhhhhh. {handle}',
				'{handle} is a sore loser'
			];
			
			var message = messages[Math.floor(Math.random() * messages.length)];

			message = message.replace('{handle}', '@' + handle);

			eb.hipchat.send(room, message, true);
		},
		plugins: function(room, message, item) {
			var plugins = Object.keys(room.plugins);
			if (!plugins.length)
				return eb.hipchat.send(room, 'This room is not running any plugins.');

			var message = 'This room is running the following plugins: ';

			plugins.forEach(function(key, i) {
				message += eb.plugin.plugins[key].name;
				if (i+1 in plugins)
					message += ', ';
			});

			eb.hipchat.send(room, message);
		},
		share: function(room, message, item) {
			eb.hipchat.send(room, 'Use this URL to install EventBot in your own room: ' + eb.config.url, true);
		},
		repo: function(room, message, item) {
			eb.hipchat.send(room, 'Repo URL: <a href="' + eb.config.repoUrl + '">' + eb.config.repoUrl + '</a>');
		},
		potato: function(room, message, item) {
			eb.hipchat.send(room, '<img src="http://transitionculture.org/wp-content/uploads/potato.jpg">');
		},
		celebrate: function(room, message, item) {
			eb.hipchat.send(room, '<img src="http://media.giphy.com/media/Is1O1TWV0LEJi/giphy.gif">');
		},
		help: function(room, message, item) {
			// Short help
			if (message != 'all') {
				eb.hipchat.send(room, "Type <code>/event help all</code> for more."
					+ "<br /><br />"
					+ "Functions:<ul>"
					+ "<li><code>/event</code> - returns a list of events</li>"
					+ "<li><code>/event create [event] at [time]</code></li>"
					+ "<li><code>/event cancel [event]</code></li>"
					+ "<li><code>/event info [event]</code></li>"
					+ "<li><code>/event add [person] to [event]</code></li>"
					+ "<li><code>/event remove [person] from [event]</code></li>"
					+ "</ul>"
					+ "<br /> Example:<ul>"
					+ "<li><code>/event create Dance Battle at 12:30pm, 4 places</code></li>"
					+ "<li><code>/event add me, @stephen_fry, and @carl_sagan to Dance Battle</code></li>"
					+ "</ul>"
				);

				return;
			}


			// Long help
			eb.hipchat.send(room, "Functions:<ul>"
				+ "<li><code>/event</code> - returns a list of events</li>"
				+ "<li><code>/event create [event] at [time]</code></li>"
				+ "<li><code>/event cancel [event]</code></li>"
				+ "<li><code>/event info [event]</code></li>"
				+ "<li><code>/event change [property] of [event] to [value]</code></li>"
				+ "<li><code>/event rename [event] to [new name]</code></li>"
				+ "<li><code>/event add [person] to [event]</code></li>"
				+ "<li><code>/event remove [person] from [event]</code></li>"
				+ "<li><code>/event join [event]</code></li>"
				+ "<li><code>/event leave [event]</code></li>"
				+ "<li><code>/event invite [person] to [event]</code></li>"
				+ "<li><code>/event accept</code> - accept an invite</li>"
				+ "<li><code>/event archive</code> - view the full event archive for this room</li>"
				+ "<li><code>/event share</code> - get EventBot for your own HipChat room</li>"
				+ "</ul>"
				+ "<br /> Command chaining:<ul>"
				+ "<li>You can chain commands as follows: <code>/event create Defeat Captain Hammer at 4:20pm | add @nph to defeat captain hammer </code></li>"
				+ "</ul>"
				+ "</ul>"
				+ "<br /> Example:<ul>"
				+ "<li><code>/event create Dance Battle at 12:30pm, 4 places</code></li>"
				+ "<li><code>/event add me, @stephen_fry, and @carl_sagan to Dance Battle</code></li>"
				+ "<li><code>/event remove @stephen_fry from Dance Battle</code></li>"
				+ "<li><code>/event change time of Dance Battle to 4pm tomorrow</code></li>"
				+ "<li><code>/event create Release the Kraken at 3pm Wednesday</code></li>"
				+ "</ul>"
			);
		},
		cmdNotFound: function(room, message, item) {
			eb.hipchat.send(room, "Error: There is no such command. Run /event help for more info.");
		}
	};
	return _this;
}