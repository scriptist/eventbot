require('sugar');

var eb;

var regex = {
	record: /^((?:@[^, -+_]+|me)(?:(?:, ?| |,? and )(?:@[^, -+_]+|me))*) as winners? of ([^@]+?)$/i,
	result: /^of ([^@]+?)$/i
}

module.exports = (function() {
	var _this = {
		name: 'Board Games',
		init: function(eventBot) {
			eb = eventBot;
		},
		getStats: function(room, substr) {
			substr = substr.toLowerCase();

			var attendees = {}
				games = 0,
				games_with_winners = 0;
			room.eventArchive.forEach(function(evt) {
				if (evt.name.toLowerCase().indexOf(substr) !== -1) {
					games++;
					if (evt.boardgames_winners) {
						games_with_winners++;
					}

					evt.people.forEach(function(handle) {
						if (!(handle in attendees)) {
							attendees[handle] = {
								games: 0,
								games_with_winners: 0,
								points: 0,
								wins: 0
							}
						}

						attendees[handle].games++;

						if (evt.boardgames_winners) {
							attendees[handle].games_with_winners++;
							if (evt.boardgames_winners.indexOf(handle) !== -1) {
								attendees[handle].points += evt.people.length / evt.boardgames_winners.length;
								attendees[handle].wins++;
							}
						}
					});
				}
			});

			Object.each(attendees, function(handle, info) {
				info.points_per_game = Math.round(info.points / info.games_with_winners * 100) / 100;
				info.wins_per_game = Math.round(info.wins / info.games_with_winners * 100) / 100;
			});

			return {
				games: games,
				games_with_winners: games_with_winners,
				attendees: attendees
			}
		},
		command: {
			record: function(room, message, item) {
				var matches = message.match(regex.record);

				if (!matches)
					return eb.hipchat.send(room, 'Syntax Error: This is the correct syntax: /event record [user(s)] as winner(s) of [event]');

				var users = eb.util.stringToHandles(matches[1], item),
					evtName = matches[2];

				var results = eb.event.getEventsBySubstring(room, evtName);

				if (results.length < 1)
					return eb.hipchat.send(room, 'Unable to find an event with that name');

				if (results.length > 1)
					return eb.hipchat.send(room, 'Found multiple events with that name');

				var evt = results[0];
				evt.boardgames_winners = users;
				eb.db.save();

				module.exports.command.result(room, 'of ' + evt.name, item);
			},
			result: function(room, message, item) {
				var matches = message.match(regex.result);

				if (!matches)
					return eb.hipchat.send(room, 'Syntax Error: This is the correct syntax: /event result of [event]');

				var results = eb.event.getEventsBySubstring(room, matches[1]);

				if (results.length < 1)
					return eb.hipchat.send(room, 'Unable to find an event with that name');

				if (results.length > 1)
					return eb.hipchat.send(room, 'Found multiple events with that name');

				var evt = results[0];

				if (!('boardgames_winners' in evt) || !evt.boardgames_winners.length)
					return eb.hipchat.send(room, 'There are no winners recorded for <strong>' + evt.name + '</strong> at ' + eb.util.timeToString(evt.time));

				var message = 'Winners of <strong>' + evt.name + '</strong> at ' + eb.util.timeToString(evt.time) + ': ';

				evt.boardgames_winners.forEach(function(handle, i) {
					message += '<i>@' + handle + '</i>';
					if (i+1 in evt.boardgames_winners)
						message += ', ';
				});

				eb.hipchat.send(room, message);
			},
			results: function(room, message, item) {
				return module.exports.command.result.apply(this, arguments);
			},
			stats: function(room, message, item) {
				var stats = _this.getStats(room, message),
					result = 'Stats for ' + (message ? '\'' + message + '\'' : 'all games' ) + ':<table>',
					handle = eb.util.getHandle(item);

				if (stats.games == 0) {
					return eb.hipchat.send(room, 'Error: No such events found');
				} else if (!(handle in stats.attendees)) {
					return eb.hipchat.send(room, 'Error: You have not played any such events');
				} else if (stats.attendees[handle].games_with_winners == 0) {
					return eb.hipchat.send(room, 'Error: Winners have not been recorded for your games of \'' + message + '\'');
				}

				result += '<tr><td><strong>Games Played:&nbsp;&nbsp;</strong></td><td> ' + stats.games + ' </td><td> (' + stats.games_with_winners + ' with recorded winners)</td></tr>';
				result += '<tr><td><strong>Games Played by you:&nbsp;&nbsp;</strong></td><td> ' + stats.attendees[handle].games + ' </td><td> (' + stats.attendees[handle].games_with_winners + ' with recorded winners)</td></tr>';
				result += '<tr><td><strong>Wins:&nbsp;&nbsp;</strong></td><td> ' + stats.attendees[handle].wins + '</td></tr>';
				result += '<tr><td><strong>Points:&nbsp;&nbsp;</strong></td><td> ' + stats.attendees[handle].points + ' </td><td> (points_awarded = player_count/winner_count) </td></tr>';
				result += '<tr><td><strong>Wins per game:&nbsp;&nbsp;</strong></td><td> ' + stats.attendees[handle].wins_per_game + '</td></tr>';
				result += '<tr><td><strong>Points per game:&nbsp;&nbsp;</strong></td><td> ' + stats.attendees[handle].points_per_game + '</td></tr>';

				result += '</table>';

				eb.hipchat.send(room, result);
			}
		}
	};
	return _this;
})();