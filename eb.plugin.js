module.exports = function(eb) {
	var _this = {
		plugins: {},
		init: function() {
			require("fs").readdirSync("./plugins").forEach(function(file) {
				if (!file.match(/\.js$/))
					return;

				var name = file.replace(/\.js$/, '');

				_this.plugins[name] = require("./plugins/" + file);

				if ('init' in _this.plugins[name] && typeof _this.plugins[name].init === 'function')
					_this.plugins[name].init(eb);
			});
		}
	};
	return _this;
}