$(document).ready(function() {
	var $template = $('.notification:last');
	$(document).on('click', 'button.add', function() {
		$template
			.clone()
				.removeClass('template')
				.insertBefore($template)
				.find(':input')
					.removeAttr('disabled');
	});
	$(document).on('click', 'button.remove', function() {
		$(this).closest('.notification').remove();
	});
	$('.saved').delay(3000).fadeOut(300);
});