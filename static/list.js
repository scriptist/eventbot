require([''], function (io) {
	var socket = io.connect();

	$(document).ready(function () {
		var container = $('.content');
		var content = $('#sidebar-content');
		var token = $('meta[name="signed-token"]').attr('content');
		var userId = $('meta[name="current-user-id"]').attr('content');
		socket.on('update', function (data) {
			content.html(data);
		});

		var createButton = $('#create-event-button');
		createButton.click(function () {
			container.addClass('create');
		});

		var createForm = $('#create-form');

		$.ajax({
			url: '/participants?signed_request=' + token, 
				type: 'get',
				dataType: 'json',
				beforeSend: function (request) {
					request.setRequestHeader('X-acpt', token);
				}
		}).done(function (participants) {
			var select = $('<select>').prop('multiple', true).attr('name', 'invites');
			for (var i = 0; i < participants.length; i++) {
				var participant = participants[i];
				if (participant.id != userId) {
					select.append($('<option>').val(participant.mention_name).text(participant.name));
				}
			}
			var fieldGroup = createForm.find('#invites-field');
			fieldGroup.find('input.text').remove();
			fieldGroup.append(select);
			select.auiSelect2();
		});

		createForm.find('.cancel').click(function () {
			container.removeClass('create');
		});

		createForm.submit(function (e) {
			e.preventDefault();

			$.ajax({
				url: '/create?signed_request=' + token, 
				type: 'post',
				contentType: 'application/json',
				data: JSON.stringify({
					name: createForm.find('[name="name"]').val(),
					time: createForm.find('[name="time"]').val(),
					places: createForm.find('[name="places"]').val(),
					invites: createForm.find('[name="invites"]').val()
				}),
				beforeSend: function (request) {
					request.setRequestHeader('X-acpt', token);
				}
			}).done(function () {
				container.removeClass('create');
			}).fail(function(xhr) {
				var error;
				if (xhr.status === 400) {
					error = xhr.responseText;
				} else {
					error = 'An error occurred.'
				}
				AJS.messages.error({
					body: error,
					fadeout: true
				});
			});
		});
	});

});