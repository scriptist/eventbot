google.load('visualization', '1.0', {'packages':['corechart']});

// Set a callback to run when the Google Visualization API is loaded.
google.setOnLoadCallback(renderChart);
$(window).on('resize', function() {
	requestAnimationFrame(renderChart);
});

function renderChart() {
	var users = {};
	var dataArray = [];
	$('.events>li:visible').each(function() {
		var handles = $(this).attr('data-people').split(',');
		for (var i = 0; i < handles.length; i++) {
			var handle = handles[i];

			if (!handle)
				return;

			if (!(handle in users))
				users[handle] = dataArray.push(['@' + handle, 0]) - 1;

			dataArray[users[handle]][1]++;
		};
	});

	dataArray.sort(function(a, b) {
		return a[0] > b[0] ? 1 : -1;
	});

	// Create the data table.
	var data = new google.visualization.DataTable();
	data.addColumn('string', 'Handle');
	data.addColumn('number', 'Events');

	data.addRows(dataArray);

	// Set chart options
	var w = $(document).width();
	var options = {
		title:'Events attended',
		legend: 'right',
		height: 500
	};

	// Instantiate and draw our chart, passing in some options.
	var chart = new google.visualization.PieChart($('.chart').empty()[0]);
	chart.draw(data, options);
}

function removeSpecialCharacters(s) {
	return s.toLowerCase();
}

$(document).on('keyup change blur focus', 'input[name=filter]', function() {
	var s = removeSpecialCharacters($(this).val());

	if (!s) {
		$('.events>li').show();
		renderChart();
		return;
	}

	$('.events>li').hide().filter(function(i, elm) {
		// Search by user
		if (s.indexOf('@') == 0) {
			return removeSpecialCharacters($(elm).find('.people').text()).indexOf(s) > -1;
		}

		return removeSpecialCharacters($(elm).find('h4').text()).indexOf(s) > -1;
	}).show();

	renderChart();
});