require('sugar');

module.exports = function(eb) {
	var _this = {
		timeToString: function(time) {
			var dateObj = Date.create(time);
			if (dateObj.is('today') && dateObj.isAfter(eb.config.eventExpiryMinutes + ' minutes ago'))
				return dateObj.format('{h}:{mm}{tt}');
			else
				return dateObj.format('{h}:{mm}{tt} {Dow} {Mon} {ord} {yyyy}');
		},
		getHandle: function(item) {
			return item.message.from.mention_name.toLowerCase();
		},
		stringToHandles: function(string, item) {
			var handles = string.match(eb.regex.userHandles);
			handles.forEach(function(handle, i) {
				if (handle == 'me')
					handles[i] = _this.getHandle(item);
				else
					handles[i] = handle.substr(1);
			});
			return handles;
		},
		clone: function(obj) {
			if (obj === null || obj === undefined)
				return obj;
			return JSON.parse(JSON.stringify(obj));
		},
		getRoomFromArchiveId: function(id) {
			var foundRoom;

			eb.rooms.forEach(function(room) {
				room.archivePages.forEach(function(archive) {
					if (archive.id == id)
						foundRoom = room;
				});
			});

			return foundRoom;
		},
		hash: function(str) {
			var hash = 0, i, chr, len;
			if (str.length == 0) return hash;
			for (i = 0, len = str.length; i < len; i++) {
				chr   = str.charCodeAt(i);
				hash  = ((hash << 5) - hash) + chr;
				hash |= 0; // Convert to 32bit integer
			}
			return hash;
		},
		getCommand: function(command, room) {
			if (command in eb.command)
				return eb.command[command];

			var cmd;

			Object.keys(room.plugins).forEach(function(key) {
				var plugin = eb.plugin.plugins[key];
				if (plugin && ('command' in plugin) && (command in plugin.command))
					cmd = plugin.command[command];
			});

			return cmd || eb.command.cmdNotFound;
		}
	};
	return _this;
}