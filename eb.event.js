require('sugar');

module.exports = function(eb) {
	function _filter(data, options) {
		var defaults = {
			filter: null
		}, events;

		settings = Object.merge(defaults, options);
		if ('events' in data)
			events = data.events;
		else
			events = data;

		if (!events || events.length == 0)
			return [];

		return events.filter(function(evt) {
			if (typeof settings.filter == 'function') {
				return settings.filter(evt);
			}
			return true;
		});
	};

	var _this = {
		// Return an HTML list of all events in a room, or all events provided
		htmlList: function(data, options) {
			var defaults = {
				categorise: true,
				extended: false
			};

			settings = Object.merge(defaults, options);
			var events = _filter(data, options);

			if (events.length == 0)
				return "No events found";

			var html = '',
				past = today = upcoming = false
				count = 0;

			events.forEach(function(evt, i) {
				if (settings.categorise) {
					if (!past && Date.create(evt.time).isPast()) {
						past = true;
						html += (i > 0 ? '<br />' : '') + '<em>Past:</em><br />';
					} else if (!today && Date.create(evt.time).isFuture() && Date.create(evt.time).is('today')) {
						today = true;
						html += (i > 0 ? '<br />' : '') + '<em>Today:</em><br />';
					} else if (!upcoming && Date.create(evt.time).isAfter('12am tomorrow')) {
						upcoming = true;
						html += (i > 0 ? '<br />' : '') + '<em>Upcoming Events:</em><br />';
					}
				}

				// Event info
				html += _this.htmlInfo(evt, 5, settings.extended) + (i < events.length ? '<br />' : '');
				count++;
			});

			if (!count)
				return 'No events found';

			return html;
		},
		// Return an HTML string with event info
		htmlInfo: function(evt, userLimit, extended) {
			userLimit = isNaN(parseInt(userLimit)) ? evt.people.length : parseInt(userLimit);

			if (!evt)
				return 'Unable to find event';

			// Name & time
			var html = '<strong>' + evt.name + '</strong> at ' + eb.util.timeToString(evt.time);

			// Attending count
			html += ' (' + evt.people.length + (evt.places ? '/' + evt.places : '') + ' attending)';

			// Attending list
			if (userLimit) {
				html += '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="' + eb.config.url + '/static/icon/attending.png"> ';

				evt.people.forEach(function(person, i) {
					if (i < userLimit || (i == userLimit && i == evt.people.length - 1))
						html += '<i>@' + person + '</i>' + (i + 1 in evt.people ? ', ' : '');
					else if (i == userLimit)
						html += 'and ' + (evt.people.length - i) + ' more';
				});
			}

			// Invitees
			if (extended) {
				var invitedHtml = '';
				var declinedHtml = '';
				if ('invites' in evt) {
					evt.invites.forEach(function(invite, i) {
						if (invite.status == 'accepted' || evt.people.indexOf(invite.user) !== -1)
							return;

						if (!('status' in invite)) {
							if (invitedHtml)
								invitedHtml += ', ';
							invitedHtml += '<i>@' + invite.user + '</i>';
						}
						if (invite.status === 'declined') {
							if (declinedHtml)
								declinedHtml += ', ';
							declinedHtml += '<i>@' + invite.user + '</i>';
						}
					});
				}

				html += '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="' + eb.config.url + '/static/icon/invited.png"> ' + invitedHtml
					 +  '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="' + eb.config.url + '/static/icon/declined.png"> ' + declinedHtml;
			}

			return html;
		},
		upcoming: function(room) {
			return _filter(room, {
				filter: function(evt) {
					return Date.create(evt.time).isFuture();
				}
			});
		},
		upcomingGlanceJson: function(room) {
			if (!room) {
				events = []
			} else {
				events = _this.upcoming(room);
			}

			return {
				label: {
					type: "html",
					value: "<strong>" + events.length + "</strong> upcoming events"
				}
			};
		},
		// Return an HTML string with the events one user is attending
		htmlMine: function(room, handle) {
			return '<em>Your events:</em><br />' + _this.htmlList(room,
				{
					categorise: false,
					filter: function(evt) {
						return evt.people.indexOf(handle) !== -1;
					}
				}
			);
		},
		// Return an HTML string with the events today
		htmlToday: function(room) {
			return '<em>Events today:</em><br />' + _this.htmlList(room,
				{
					categorise: false,
					filter: function(evt) {
						return Date.create(evt.time).is('today');
					}
				}
			);
		},
		// Create a new event
		create: function(room, name, time, places) {
			if (!time)
				return 'Unable to parse date';

			if (!name)
				return 'Name is required';

			if (!Date.create(time).isValid())
				return 'Unrecognised time "' + value + '" (see http://sugarjs.com/dates for valid formats)';
			// Sometimes "now" will become past due to the time between creating & testing it
			if (Date.create(time).addSeconds(10).isPast())
				return 'You cannot create events in the past';

			if (_this.getEventByName(room, name) !== null)
				return 'An event named "' + name + '" already exists';

			var evt = {
				name: name,
				time: time,
				places: places,
				people: [],
				notifications: eb.util.clone(room.notifications)
			}

			// Check if notifications have already passed
			evt.notifications.forEach(function(notification) {
				if (Date.create(notification.number + ' ' + notification.unit + ' from now').isAfter(evt.time))
					notification.notified = true;
			});

			room.events.push(evt);

			_this.sort(room);
			eb.db.save();

			return evt;
		},
		// Cancel an existing event
		cancel: function(room, evtName) {
			var indices = _this.getEventIndicesBySubstring(room, evtName);

			if (indices.length < 1)
				return 'Unable to find an event with that name';

			if (indices.length > 1)
				return 'Found multiple events with that name';

			var evt = room.events.splice(indices[0], 1)[0];

			eb.db.save();

			return evt;
		},
		// Change a property of an event
		change: function(room, evtName, property, value) {
			var evts = _this.getEventsBySubstring(room, evtName);

			if (evts.length < 1)
				return 'Unable to find an event with that name';

			if (evts.length > 1)
				return 'Found multiple events with that name';

			var evt = evts[0];

			property = property.toLowerCase();
			if (property != 'time' && property != 'name' && property != 'places')
				return property + ' is not a valid property.';

			if (property == 'time') {
				if (!Date.create(value).isValid())
					return 'Unrecognised time "' + value + '" (see http://sugarjs.com/dates for valid formats)';
				if (Date.create(value).addSeconds(10).isPast())
					return 'You cannot create events in the past';
			}

			evt[property] = value;

			_this.sort(room);
			eb.db.save();

			return evt;
		},
		// Add users to an event
		addUsers: function(room, evtName, users) {
			var evts = _this.getEventsBySubstring(room, evtName);

			if (evts.length < 1)
				return 'Unable to find an event with that name';

			if (evts.length > 1)
				return 'Found multiple events with that name';

			var evt = evts[0];

			if (evt.places !== null && evt.places - evt.people.length == 0)
				return 'This event is full';

			users.forEach(function(user) {
				if (evt.places !== null && evt.places - evt.people.length == 0)
					return true;

				user = user.toLowerCase();
				if (evt.people.indexOf(user) === -1) {
					evt.people.push(user);
				}
			});

			eb.db.save();

			return evt;
		},
		// Remove users from an event
		removeUsers: function(room, evtName, users) {
			var evts = _this.getEventsBySubstring(room, evtName);

			if (evts.length < 1)
				return 'Unable to find an event with that name';

			if (evts.length > 1)
				return 'Found multiple events with that name';

			var evt = evts[0];

			users.forEach(function(user) {
				user = user.toLowerCase();
				if (evt.people.indexOf(user) !== -1) {
					evt.people.splice(evt.people.indexOf(user), 1);
				}
			});

			eb.db.save();

			return evt;
		},
		// Invite users to an event
		inviteUsers: function(room, evtName, users, from) {
			var evts = _this.getEventsBySubstring(room, evtName);

			if (evts.length < 1)
				return 'Unable to find an event with that name';

			if (evts.length > 1)
				return 'Found multiple events with that name';

			var evt = evts[0];

			if (!('invites' in evt))
				evt.invites = [];

			users.forEach(function(user) {
				user = user.toLowerCase();

				// First overwrite existing invites
				var result = _this.getInvite(room, user);
				if (result)
					result.invite.status = 'overwritten';

				// Then create new invite
				evt.invites.push({
					user: user,
					from: from
				});
			});

			eb.db.save();

			return evt;
		},
		// Get common users from past events
		getUsersFromPastEvents: function(room, likeEvt) {
			var attendees = {};
			var evtList = _this.getArchivedEventsBySubstring(room, likeEvt, true);

			evtList.forEach(function(evt) {
				evt.people.forEach(function(handle) {
					if (!(handle in attendees))
						attendees[handle] = 0;

					attendees[handle]++;
				});
			});

			var ordered = [];
			for (handle in attendees) {
				ordered.push({
					handle: handle,
					count: attendees[handle]
				});
			}

			ordered.sort(function(a, b) {
				return b.count - a.count;
			});

			var users = [];
			for (var i = 0; i < ordered.length; i++) {
				users.push(ordered[i].handle);
			}

			return users;
		},
		// Return an event to which the user is invited
		getInvite: function(room, user) {
			var resultEvt = null;
			var resultInvite = null;
			room.events.forEach(function(evt, i) {
				if ('invites' in evt) {
					evt.invites.forEach(function(invite) {
						if (invite.user == user && !invite.status) {
							resultEvt = evt;
							resultInvite = invite;
						}
					});
				}
			});

			if (!resultEvt || !resultInvite)
				return;

			return {
				evt: resultEvt,
				invite: resultInvite
			};
		},
		// Accept an invitation
		acceptInvite: function(room, user) {
			var result = _this.getInvite(room, user);
			if (!result)
				return 'You have not been invited to anything';

			result.invite.status = 'accepted';

			return _this.addUsers(room, result.evt.name, [user]);
		},
		// Decline an invitation
		declineInvite: function(room, user) {
			var result = _this.getInvite(room, user);
			if (!result)
				return 'You have not been invited to anything';

			result.invite.status = 'declined';
			eb.db.save();

			return result;
		},
		// Search for an event by substring matching, return the event object
		getEventsBySubstring: function(room, substr, noPerfectMatch, eventArchive) {
			var indices = _this.getEventIndicesBySubstring(room, substr, noPerfectMatch, eventArchive),
				result = [];

			var eventArray = eventArchive ? room.eventArchive : room.events;

			indices.forEach(function(index) {
				result.push(eventArray[index]);
			});

			return result;
		},
		// Search for an event by substring matching, return the event index
		getEventIndicesBySubstring: function(room, substr, noPerfectMatch, eventArchive) {
			var perfectMatch = _this.getEventIndexByName(room, substr);

			if (perfectMatch !== null && !noPerfectMatch)
				return [perfectMatch];

			var result = [];
			substr = substr.toLowerCase();

			var eventArray = eventArchive ? room.eventArchive : room.events;

			eventArray.forEach(function(evt, i) {
				if (evt.name.toLowerCase().indexOf(substr) !== -1) {
					result.push(i);
				}
			});

			return result;
		},
		// Search for an event by substring matching, return the event object
		getArchivedEventsBySubstring: function(room, substr, noPerfectMatch) {
			return _this.getEventsBySubstring(room, substr, noPerfectMatch, true);
		},
		// Search for an event by substring matching, return the event object
		getArchivedEventIndicesBySubstring: function(room, substr, noPerfectMatch) {
			return _this.getEventIndicesBySubstring(room, substr, noPerfectMatch, true);
		},
		// Search for an event by name, return the event object
		getEventByName: function(room, name) {
			var i = _this.getEventIndexByName(room, name);
			if (i === null)
				return null;

			return room.events[i];
		},
		// Search for an event by name, return the event index
		getEventIndexByName: function(room, name) {
			var index = null;

			name = name.toLowerCase();
			room.events.forEach(function(evt, i) {
				if (evt.name.toLowerCase() == name) {
					index = i;
					return true;
				}
			});

			return index;
		},
		// Sort events
		sort: function(room) {
			room.events.sort(function(a, b) {
				return a.time - b.time;
			});

			return room.events;
		}
	};
	return _this;
}
