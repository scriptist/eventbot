// Event scheduling bot for HipChat using node.js

var eb = {
	rooms: []
}

files = require("glob").sync("eb.*.js");
files.forEach(function(filename) {
	var module = filename.split('.')[1],
		file = './' + filename.replace(/\.js$/, '');
	eb[module] = require(file)(eb);
});

eb.init();
