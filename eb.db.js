// Save to DB
var mongo = require('mongodb');

module.exports = function(eb) {
	var _this = {
		collection: 'db',
		name: 'dbString',
		save: function() {
			if (!eb.config.mongoEnabled)
				return;

			mongo.Db.connect(eb.config.mongoUri, function (err, db) {
				db.collection(_this.collection).update(
					// Condition
					{
						name:
						_this.name
					},
					// Operation
					{
						$set: {
							name: _this.name,
							versionInt: eb.config.versionInt,
							rooms: eb.rooms
						}
					},
					// Options
					{
						safe: true,
						upsert: true
					},
					function(err) {
						if (err)
							console.log('Error saving: ' + err);
						else
							console.log('Saved successfully');
					}
				);
			});
		},
		load: function(callback) {
			if (!eb.config.mongoEnabled)
				return typeof callback == 'function' && callback();

			mongo.Db.connect(eb.config.mongoUri, function (err, db) {
				db.collection(_this.collection).find({name: _this.name}).limit(1).toArray(function(err, docs) {
					if (docs.length == 0)
						return;

					eb.rooms = docs[0].rooms;

					typeof callback == 'function' && callback();
				});
			});
		}
	};
	return _this;
}